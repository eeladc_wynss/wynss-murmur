package org.nansa.wynss.server.murmur.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.nansa.wynss.server.common.entity.QuickSearchEntity;
import org.nansa.wynss.server.common.entity.manager.QuickSearchEntityManager;
import org.nansa.wynss.server.common.entity.repo.QuickSearchEntityRepo;

public abstract class AutoTimeTrackManager<Id extends Serializable, Entity extends QuickSearchEntity<Id>,Repository extends QuickSearchEntityRepo<Entity, Id>> extends QuickSearchEntityManager<Id,Entity,Repository> {

	public AutoTimeTrackManager(Repository repo) {
		super(repo);
	}

	@Override
	public Entity save(Entity item) {
		autoSetTimestamp(item);
		return super.save(item);
	}

	private void autoSetTimestamp(Entity item) {
		if(item.getTimestamp() == null) item.setTimestamp(new Date());
	}

	@Override
	public List<Entity> saveAll(List<Entity> items) {
		items.forEach(this::autoSetTimestamp);
		return super.saveAll(items);
	}



}
