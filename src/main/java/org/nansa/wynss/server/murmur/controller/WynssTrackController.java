package org.nansa.wynss.server.murmur.controller;

import org.nansa.wynss.server.common.application.execution.Executor;
import org.nansa.wynss.server.common.application.execution.Logic;
import org.nansa.wynss.server.common.controller.WynssController;
import org.nansa.wynss.server.common.ws.Request;
import org.nansa.wynss.server.common.ws.Response;
import org.nansa.wynss.server.common.ws.status.KnownError;
import org.nansa.wynss.server.murmur.document.Product;
import org.nansa.wynss.server.murmur.manager.ProductManager;
import org.nansa.wynss.server.murmur.response.StandardResponse;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
@RequestMapping(value="/track", produces = "application/json;charset=UTF-8")
public class WynssTrackController extends WynssController {

	@PostMapping(path = "/1")
	public Response<StandardResponse> product(@RequestBody Request<Product> productRequest){
		return new Executor<Product,ProductManager,StandardResponse>("/1")
				.withRequest(productRequest)
				.ifFails(KnownError.TRACK_FAILED)
				.withLogic(Logic.saveAndMap(Product::toResponse))
				.run(ProductManager.class);
	}
	
}
