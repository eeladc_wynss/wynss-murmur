package org.nansa.wynss.server.murmur.response;

import org.nansa.wynss.server.common.utils.EntityUtils;

public class StandardResponse {

	private String c1;
	private String a1;
	private String a2;
	private String a3;
	private String a4;
	private String a5;
	private String a6;
	private String a7;
	private String a8;
	private String a9;
	private String a10;
	private boolean a11;
	private boolean a12;
	private boolean a13;
	private boolean a14;
	private boolean a15;
	private Long a16;
	private Long a17;
	private Long a18;
	/**
	 * @return the c1
	 */
	public String getC1() {
		return c1;
	}
	/**
	 * @param c1 the c1 to set
	 */
	public void setC1(String c1) {
		this.c1 = c1;
	}
	/**
	 * @return the a1
	 */
	public String getA1() {
		return a1;
	}
	/**
	 * @param a1 the a1 to set
	 */
	public void setA1(String a1) {
		this.a1 = a1;
	}
	/**
	 * @return the a2
	 */
	public String getA2() {
		return a2;
	}
	/**
	 * @param a2 the a2 to set
	 */
	public void setA2(String a2) {
		this.a2 = a2;
	}
	/**
	 * @return the a3
	 */
	public String getA3() {
		return a3;
	}
	/**
	 * @param a3 the a3 to set
	 */
	public void setA3(String a3) {
		this.a3 = a3;
	}
	/**
	 * @return the a4
	 */
	public String getA4() {
		return a4;
	}
	/**
	 * @param a4 the a4 to set
	 */
	public void setA4(String a4) {
		this.a4 = a4;
	}
	/**
	 * @return the a5
	 */
	public String getA5() {
		return a5;
	}
	/**
	 * @param a5 the a5 to set
	 */
	public void setA5(String a5) {
		this.a5 = a5;
	}
	@Override
	public String toString() {
		return EntityUtils.toString(this);
	}
	
	@Override
	public int hashCode() {
		return EntityUtils.hashCode(this);
	}
	
	@Override
	public boolean equals(Object other) {
		return EntityUtils.equals(this, other);
	}
	/**
	 * @return the a6
	 */
	public String getA6() {
		return a6;
	}
	/**
	 * @param a6 the a6 to set
	 */
	public void setA6(String a6) {
		this.a6 = a6;
	}
	/**
	 * @return the a7
	 */
	public String getA7() {
		return a7;
	}
	/**
	 * @param a7 the a7 to set
	 */
	public void setA7(String a7) {
		this.a7 = a7;
	}
	/**
	 * @return the a8
	 */
	public String getA8() {
		return a8;
	}
	/**
	 * @param a8 the a8 to set
	 */
	public void setA8(String a8) {
		this.a8 = a8;
	}
	/**
	 * @return the a9
	 */
	public String getA9() {
		return a9;
	}
	/**
	 * @param a9 the a9 to set
	 */
	public void setA9(String a9) {
		this.a9 = a9;
	}
	/**
	 * @return the a10
	 */
	public String getA10() {
		return a10;
	}
	/**
	 * @param a10 the a10 to set
	 */
	public void setA10(String a10) {
		this.a10 = a10;
	}
	/**
	 * @return the a11
	 */
	public boolean isA11() {
		return a11;
	}
	/**
	 * @param a11 the a11 to set
	 */
	public void setA11(boolean a11) {
		this.a11 = a11;
	}
	/**
	 * @return the a12
	 */
	public boolean isA12() {
		return a12;
	}
	/**
	 * @param a12 the a12 to set
	 */
	public void setA12(boolean a12) {
		this.a12 = a12;
	}
	/**
	 * @return the a13
	 */
	public boolean isA13() {
		return a13;
	}
	/**
	 * @param a13 the a13 to set
	 */
	public void setA13(boolean a13) {
		this.a13 = a13;
	}
	/**
	 * @return the a14
	 */
	public boolean isA14() {
		return a14;
	}
	/**
	 * @param a14 the a14 to set
	 */
	public void setA14(boolean a14) {
		this.a14 = a14;
	}
	/**
	 * @return the a18
	 */
	public Long getA18() {
		return a18;
	}
	/**
	 * @param a18 the a18 to set
	 */
	public void setA18(Long a18) {
		this.a18 = a18;
	}
	/**
	 * @return the a15
	 */
	public boolean isA15() {
		return a15;
	}
	/**
	 * @param a15 the a15 to set
	 */
	public void setA15(boolean a15) {
		this.a15 = a15;
	}
	/**
	 * @return the a16
	 */
	public Long getA16() {
		return a16;
	}
	/**
	 * @param a16 the a16 to set
	 */
	public void setA16(Long a16) {
		this.a16 = a16;
	}
	/**
	 * @return the a17
	 */
	public Long getA17() {
		return a17;
	}
	/**
	 * @param a17 the a17 to set
	 */
	public void setA17(Long a17) {
		this.a17 = a17;
	}
	
	
}
