package org.nansa.wynss.server.murmur.store.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.nansa.wynss.server.common.application.ApplicationConstant;
import org.nansa.wynss.server.common.exception.WynssStartupException;
import org.nansa.wynss.server.common.ws.status.KnownError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@PropertySource("classpath:track.properties")
@EnableElasticsearchRepositories(basePackages = {ApplicationConstant.TRACK_ENTITY_BASE_PACKAGE})
@ComponentScan(basePackages = {ApplicationConstant.TRACK_MANAGER_BASE_PACKAGE})
public class TrackStoreConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(TrackStoreConfiguration.class);

	
	@Autowired
	Environment envt;
	
	public HttpHost[] getAddress() {
		String hosts = envt.getProperty("track.store.hosts");
		LOG.info("Track store host(s): {}",hosts);
		return Arrays.stream(hosts.split(",")).map(this::parseUrl).toArray(s->new HttpHost[s]);
	}


	private HttpHost parseUrl(String url) {
		try {
			URL urlObj = new URL(url);
			return new HttpHost(urlObj.getHost(),urlObj.getPort(), urlObj.getProtocol());
		} catch (MalformedURLException e) {
			throw new WynssStartupException(KnownError.FAILED_TO_START, e, "Track store hosts");
		}
	}
	
	
	public String getClusterName() {
		return envt.getProperty("track.store.cluster", "elasticsearch");
	}

	@SuppressWarnings("resource")
	@Bean(destroyMethod = "close")
	public RestHighLevelClient client() throws UnknownHostException {
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(getAddress()));
      return client;
	}
    
    @Bean
    public ElasticsearchRestTemplate elasticsearchTemplate() {
        try {
			return new ElasticsearchRestTemplate(client());
		} catch (UnknownHostException e) {
			throw new WynssStartupException(KnownError.FAILED_TO_START, e, "Track store");
		}
    }

}
