package org.nansa.wynss.server.murmur.document;


import org.nansa.wynss.server.common.application.ApplicationConstant;
import org.nansa.wynss.server.common.entity.QuickSearchEntity;
import org.nansa.wynss.server.common.utils.NumberUtils;
import org.springframework.data.annotation.Id;

public abstract class BasicDocument<RequestObject,ResponseObject> extends QuickSearchEntity<Long>{
	
	@Id private Long id;
	
	public abstract ResponseObject toResponse() ;

	@Override
	public Long newId() {
		return NumberUtils.unique();
	}


	@Override
	public Long getId() {
		return this.id;
	}


	@Override
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String getIdFieldName() {
		return "id";
	}

	@Override
	public String indexName() {
		return ApplicationConstant.TRACK_INDEX_NAME;
	}

}
