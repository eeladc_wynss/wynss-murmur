package org.nansa.wynss.server.murmur.document;

import org.nansa.wynss.server.common.application.ApplicationConstant;
import org.nansa.wynss.server.murmur.response.StandardResponse;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = ApplicationConstant.TRACK_INDEX_NAME, type = Product.TYPE)
public class Product extends BasicDocument<Product,StandardResponse> {

	protected static final String TYPE = "Product";
	
	private String context;
	private String productName;
	private long branchId;
	private long supplierId;
	private String brandName;
	private String promoCode;
	private String price;
	private String quantity;
	private boolean premiumPromotion;
	/**
	 * @return the context
	 */
	public String getContext() {
		return context;
	}
	/**
	 * @param context the context to set
	 */
	public void setContext(String context) {
		this.context = context;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the promoCode
	 */
	public String getPromoCode() {
		return promoCode;
	}
	/**
	 * @param promoCode the promoCode to set
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	@Override
	public StandardResponse toResponse() {
		StandardResponse resp = new StandardResponse();
		resp.setC1(this.getContext());
		resp.setA1(this.getProductName());
		resp.setA16(this.getBranchId());
		resp.setA17(this.getSupplierId());
		resp.setA2(this.getBrandName());
		resp.setA3(this.getPrice());
		resp.setA4(this.getQuantity());
		return resp;
	}
	/**
	 * @return the branchId
	 */
	public long getBranchId() {
		return branchId;
	}
	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}
	/**
	 * @return the supplierId
	 */
	public long getSupplierId() {
		return supplierId;
	}
	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the premiumPromotion
	 */
	public boolean isPremiumPromotion() {
		return premiumPromotion;
	}
	/**
	 * @param premiumPromotion the premiumPromotion to set
	 */
	public void setPremiumPromotion(boolean premiumPromotion) {
		this.premiumPromotion = premiumPromotion;
	}
	
	@Override
	public String type() {
		return TYPE;
	}
	
}
