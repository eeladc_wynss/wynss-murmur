package org.nansa.wynss.server.murmur.manager;

import org.nansa.wynss.server.common.entity.manager.QuickSearchEntityManager;
import org.nansa.wynss.server.murmur.document.Product;
import org.nansa.wynss.server.murmur.document.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductManager extends QuickSearchEntityManager<Long,Product,ProductRepo>{
	
	@Autowired
	public ProductManager(ProductRepo repo) {
		super(repo);
	}

}
