package org.nansa.wynss.server.murmur.exception;

import java.net.BindException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nansa.wynss.server.common.exception.WynssException;
import org.nansa.wynss.server.common.exception.WynssExceptionDictionary;
import org.nansa.wynss.server.common.exception.WynssExceptionTranslator;
import org.nansa.wynss.server.common.exception.WynssValidationFailedException;
import org.nansa.wynss.server.common.ws.status.ErrorDetail;
import org.nansa.wynss.server.common.ws.status.KnownError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.util.WebUtils;



@ControllerAdvice
public class WynssExceptionHandler {
    
	 public static final String PAGE_NOT_FOUND_LOG_CATEGORY = "org.springframework.web.servlet.PageNotFound";

	  
	  protected static final Logger pageNotFoundLogger = LoggerFactory.getLogger(PAGE_NOT_FOUND_LOG_CATEGORY);
	  protected final Logger logger = LoggerFactory.getLogger(WynssExceptionHandler.class);


	  @ExceptionHandler({
	      HttpRequestMethodNotSupportedException.class,
	      HttpMediaTypeNotSupportedException.class,
	      HttpMediaTypeNotAcceptableException.class,
	      MissingPathVariableException.class,
	      MissingServletRequestParameterException.class,
	      ServletRequestBindingException.class,
	      ConversionNotSupportedException.class,
	      TypeMismatchException.class,
	      HttpMessageNotReadableException.class,
	      HttpMessageNotWritableException.class,
	      MethodArgumentNotValidException.class,
	      MissingServletRequestPartException.class,
	      BindException.class,
	      NoHandlerFoundException.class,
	      AsyncRequestTimeoutException.class
	    })
	  @Nullable
	  public final ResponseEntity<Object> handleException(Exception ex, WebRequest request) throws Exception {
	   return handleExceptionInternal(ex, WynssExceptionTranslator.processException(ex),request);
	  }

	  
	  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
	      HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    pageNotFoundLogger.warn(ex.getMessage());

	    Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
	    if (!CollectionUtils.isEmpty(supportedMethods)) {
	      headers.setAllow(supportedMethods);
	    }
	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
	      HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    List<MediaType> mediaTypes = ex.getSupportedMediaTypes();
	    if (!CollectionUtils.isEmpty(mediaTypes)) {
	      headers.setAccept(mediaTypes);
	    }

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(
	      HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleMissingPathVariable(
	      MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleMissingServletRequestParameter(
	      MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleServletRequestBindingException(
	      ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleConversionNotSupported(
	      ConversionNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleTypeMismatch(
	      TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleHttpMessageNotReadable(
	      HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleHttpMessageNotWritable(
	      HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleMethodArgumentNotValid(
	      MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleMissingServletRequestPart(
	      MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }

	  
	  protected ResponseEntity<Object> handleBindException(
	      BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }


	  protected ResponseEntity<Object> handleNoHandlerFoundException(
	      NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    return handleExceptionInternal(ex, null, headers, status, request);
	  }


	  @Nullable
	  protected ResponseEntity<Object> handleAsyncRequestTimeoutException(
	      AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {

	    if (webRequest instanceof ServletWebRequest) {
	      ServletWebRequest servletWebRequest = (ServletWebRequest) webRequest;
	      HttpServletResponse response = servletWebRequest.getResponse();
	      if (response != null && response.isCommitted()) {
	        if (logger.isWarnEnabled()) {
	          logger.warn("Async request timed out");
	        }
	        return null;
	      }
	    }

	    return handleExceptionInternal(ex, null, headers, status, webRequest);
	  }


	  protected ResponseEntity<Object> handleExceptionInternal(
	      Exception ex, @Nullable ErrorDetail body, HttpHeaders headers, HttpStatus status, WebRequest request) {

	    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
	      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
	    }
	    return new ResponseEntity<>(body, headers, status);
	  }
	  
	  protected ResponseEntity<Object> handleExceptionInternal(
		      Exception ex, @Nullable Pair<ErrorDetail,HttpStatus> body, WebRequest request) {

		    HttpStatus httpStatus = body.getRight();
			if (HttpStatus.INTERNAL_SERVER_ERROR.equals(httpStatus)) {
		      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		    }
		    return new ResponseEntity<>(body.getLeft(), new HttpHeaders(), httpStatus);
		  }
    
    @ExceptionHandler(WynssException.class)
    public ResponseEntity<Object> handleWynssException(WynssException ex) {
    	Pair<ErrorDetail, HttpStatus> classifiedError=WynssExceptionDictionary.classifyAndAddErrors(ex);
       return newResponseEntity(classifiedError);
    }
    
    @ExceptionHandler(WynssValidationFailedException.class)
    public ResponseEntity<Object> handleWynssValidationFailedException(WynssValidationFailedException ex) {	
       return newResponseEntity(ex.getError(),HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleException(RuntimeException ex,WebRequest req) {	
    	Pair<ErrorDetail, HttpStatus> classifiedError=WynssExceptionDictionary.classifyAndAddErrors(new WynssException(KnownError.SYSTEM_FAILURE, ex,  "Unknown RT Exception"));
       return newResponseEntity(classifiedError);
    }

	private ResponseEntity<Object> newResponseEntity(Pair<ErrorDetail, HttpStatus> classifiedError) {
		return newResponseEntity(classifiedError.getLeft(),classifiedError.getRight());
	}
	
	private ResponseEntity<Object> newResponseEntity(ErrorDetail errorDetail,HttpStatus status) {
		return new ResponseEntity<>(errorDetail,new HttpHeaders(),status);
	}
    
    @ExceptionHandler(HttpMessageConversionException.class)
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageConversionException e,WebRequest req) {	
       return newResponseEntity(new WynssException(KnownError.SERIALISATION_EXCEPTION, e, "MessageNotReadableException").getError(),HttpStatus.BAD_REQUEST);
    }
   
}
