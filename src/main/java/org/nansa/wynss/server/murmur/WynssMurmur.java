package org.nansa.wynss.server.murmur;

import org.nansa.wynss.server.common.application.ApplicationRegister;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = {"org.nansa.wynss.server.track" ,"org.nansa.wynss.server.track.manager"})
@EnableAsync
@EnableScheduling
public class WynssMurmur {

	public static void main(String[] args) throws Exception {
		new ApplicationRegister(SpringApplication.run(WynssMurmur.class, args));
	}

	
}
