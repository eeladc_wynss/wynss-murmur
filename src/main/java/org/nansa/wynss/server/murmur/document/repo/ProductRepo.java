package org.nansa.wynss.server.murmur.document.repo;

import org.nansa.wynss.server.common.entity.repo.QuickSearchEntityRepo;
import org.nansa.wynss.server.murmur.document.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends QuickSearchEntityRepo<Product, Long>{

}
