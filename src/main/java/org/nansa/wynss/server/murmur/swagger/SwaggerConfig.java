package org.nansa.wynss.server.murmur.swagger;


import java.util.Collections;

import org.nansa.wynss.server.common.application.ApplicationConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)  
		          .select()                                  
		          .apis(RequestHandlerSelectors.basePackage(ApplicationConstant.TRACK_ACTUATOR_BASE_PACKAGE))
		          .paths(PathSelectors.any())  
		          .build()
		          .apiInfo(apiInfo());
	}
	 
	private ApiInfo apiInfo() {
	    
		return new ApiInfo(
	      "Wynss Track API", 
	      "Tracking APIs for Wynss server-side", 
	      "API TOS", 
	      "Terms of service", 
	      ApplicationConstant.COMPANY_CONTACT,
	      "", "", Collections.emptyList());
	}
}