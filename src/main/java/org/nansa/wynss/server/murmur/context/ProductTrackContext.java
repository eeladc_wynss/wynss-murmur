package org.nansa.wynss.server.murmur.context;

public enum ProductTrackContext implements TrackContext{
    S("Search"),
    LS("LocationSearch"),
    PSS("ProductSelectFromSearch"),
    PBS("ProductBuyFromSearch"),
    PBC("ProductBuyCancel"),
    PBW("ProductBuyFromWishlist"),
    PBO("ProductBuyComplete"),
    PSW("ProductSelectFromWishList")

    ;

    public String getContextName() {
        return contextName;
    }

    private final String contextName;
    ProductTrackContext(String contextName){
        this.contextName = contextName;
    }

}
