package org.nansa.wynss.server.murmur.context;

public class ComplexTrackContext<T> implements TrackContext{

    private T data;

    public ComplexTrackContext(T data){
        this.data = data;
    }

    @Override
    public String getContextName() {
        return data.getClass().getSimpleName();
    }
}
