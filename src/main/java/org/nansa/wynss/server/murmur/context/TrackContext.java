package org.nansa.wynss.server.murmur.context;

public interface TrackContext {
	
    public String getContextName();
    
}
